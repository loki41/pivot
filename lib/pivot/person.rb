module Pivot
  class Person

    attr_accessor :email, :first_name, :last_name, :items

    def initialize person
      @email = person[:email]
      @first_name = person[:first_name]
      @last_name = person[:last_name]
      
      @items = []
    end

    def add_item item, &block
      @items.push(item)
      item.assignee = @email

      if block
        block.call
      end
    end
  end
end