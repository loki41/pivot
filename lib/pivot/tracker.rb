module Pivot
  class Tracker
    def self.count items
      return items.size
    end

    def self.items_for items, email
      results = items.map { |item| item if item[:assignee] == email }
      return results.compact
    end

    def self.item_for items, email
      results = items_for items, email
      return results.last
    end

    def self.project_count items
      projects = items.map { |item| item[:name] }
      return projects.uniq.size > 1
    end

    def self.pivoted? items, email
      results = items_for items, email

      return results.size > 1 ? project_count(items) : false
    end

    def self.pivoted_items items
      pivoted = []

      items.reverse.each do |item|
        pivoted_assignees = pivoted.map { |item| item[:assignee] }

        if !pivoted_assignees.include?(item[:assignee])
          pivoted.push(item)
        end
      end

      return pivoted
    end

    def self.total_points items, email=''
      if !email.empty?
        items = items_for items, email[:assignee]
      else 
        items = pivoted_items items
      end

      count = items.map{ |item| item[:points] }.sum

      return count
    end

    def self.unique_assignees items
      assignees = items.map { |item| item[:assignee] }.uniq

      return assignees
    end
  end
end