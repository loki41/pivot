module Pivot
  class Item

    attr_accessor :assignee, :name, :points

    def initialize(item)
      @name = item[:name]
      @assignee = item[:assignee]
      @points = item[:points]
    end

    def project_code
      code = @name.split('-')[0]
      return code
    end

    def + other
      return self.points + other.points
    end

    def valid?
      code = project_code

      # METHOD ONE - The easy hard coded method
      # result = code.match(/EREC|AZR/)

      # METHOD THREE - Rails only way
      # result = code.length >= 3 && code.length <= 4 ? true : false

      # METHOD THREE - Test project code for string length
      result = code.match(/^([A-Z]{3,4})$/)

      return result
    end
  end
end